from app.lib.schema import get_schema
from flask import Flask, jsonify, request
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import *
from sqlalchemy_sqlschema import maintain_schema
from flask_marshmallow import Marshmallow 

app = Flask(__name__)

engine = create_engine('postgresql+psycopg2://averages:averages@localhost:6543/test1')
Session = sessionmaker(bind=engine)
session = Session()

ma = Marshmallow(app)

def user_schema(request):
    def inner_decorator(f):
        def wrapped(*args, **kwargs):

            import os, json

            base_path = os.path.dirname(os.path.realpath(__file__))

            with open(base_path + '/schemas.json') as file:
                data = json.load(file)
                domain = request.headers.get('Host').split('.')[0]
                    
                obj = next((item for item in data if item["domain"] == domain), None)

                with maintain_schema(obj["schema"], session):
                    print(f"SCHEMA: {obj['schema']}")
                    return f(*args, **kwargs)
        wrapped.__name__ = f.__name__
        return wrapped
    return inner_decorator



# def get_all_users():

#     return session.query(User).all()
    

@app.get('/um')
@user_schema(request)
def um():

    data = session.query(User).all()

    result = UserSchema(many=True).dump(data)

    return jsonify(data=result)


@app.get('/dois')
@user_schema(request)
def dois():

    data = session.query(User).all()

    result = UserSchema(many=True).dump(data)

    return jsonify(data=result)