
def get_schema(request):

    import os, json

    base_path = os.path.dirname(os.path.realpath(__file__))

    with open(base_path + '/schemas.json') as file:
        data = json.load(file)
        domain = request.headers.get('Host').split('.')[0]
            
        obj = next((item for item in data if item["domain"] == domain), None)
        

        if not obj:
            return "um"
        
        return obj["schema"]