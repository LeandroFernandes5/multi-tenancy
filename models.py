
import sqlalchemy as sa
from sqlalchemy.sql import func
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
 
    __tablename__ = 'users'

    id = sa.Column('id', sa.Integer, primary_key=True)
    email = sa.Column('email', sa.String(128), index=True, unique=True, nullable=False)
    password = sa.Column('password', sa.String(128), nullable=False, unique=False)
    name = sa.Column('name', sa.String(64), nullable=True, unique=False)
    role = sa.Column('role', sa.String(64), nullable=False, unique=False, server_default='User')
    passwordResetToken = sa.Column('passwordResetToken', sa.String(128), nullable=True,  unique=False)
    imageURL = sa.Column('imageURL', sa.String(128), nullable=True, unique=False)
    status = sa.Column('status', sa.String(64), nullable=False, unique=False, server_default='Pending')
    createdDate = sa.Column('createdDate', sa.DateTime(timezone=True), nullable=False,  unique=False, server_default=func.now())
    modifiedDate = sa.Column('modifiedDate', sa.DateTime(timezone=True), nullable=True,  unique=False, onupdate=func.now())


from marshmallow import Schema, fields


class UserSchema(Schema):
    id = fields.Int()
    email = fields.Email()
    password = fields.String(load_only=True) # Write only, will never be returned on the JSON output
    name = fields.String()
    role = fields.String() 
    imageURL = fields.String() 
    passwordResetToken = fields.String() 
    status  = fields.String()
    createdDate = fields.DateTime(load_only=True)
    modifiedDate = fields.DateTime(load_only=True)
